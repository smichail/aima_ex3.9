CC=gcc
CFLAGS=-I.

DEPS = mac_aima_h.h
OBJ = mac_aima.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

mac_aima: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean: 
	-rm mac_aima $(OBJ)