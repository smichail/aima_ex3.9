/*
 * mac_aima_h.h 
 *  Created on: 		21 Mar 2020
 *      Author: 		S. Michail
 *      Description: 	Missionaries and cannibals problem solution, implemented
 *						in C. Header file.
 */
#ifndef MAC_AIMA_H_H_
#define MAC_AIMA_H_H_

#include <stdio.h>

#define WIN  1
#define LOSS 0
#define TRUE  1
#define FALSE 0
#define MISSIONARIES 3
#define CANNIBALS 	 3

void mac_algo();
void print_status(char* p0, char* p1);
int win_condition();

#endif /* MAC_AIMA_H_H_ */