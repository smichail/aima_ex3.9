/*
 * mac_aima.c
 *  Created on: 		21 Mar 2020
 *      Author: 		S. Michail
 *      Description: 	Missionaries and cannibals problem solution, implemented
 *						in C.
 */
#include "mac_aima_h.h"

/*
 * r_mn, r_cn: Remaining missionaries/cannibals, yet to cross the river
 * a_mn, a_cn: Missionaries/cannibals that made it across
 * boat_s: Boat side
 * boat_p: # of boat passengers
 */
int r_mn = MISSIONARIES;
int r_cn = CANNIBALS;
int a_mn = 0, a_cn =0, boat_s = 0, boat_p = 0;

void print_status(char* p0, char* p1) {
	printf("\n\n\n");
	for(int i = 0; i < a_mn; i++) {
		printf("M");
	}
	for(int i = 0; i < a_mn; i++) {
		printf("C");
	}

	if (boat_s == 0) {
		printf("	~~~~~WATER~~~~~<BOAT(%s,%s)>	", p0, p1);
	} else {
		printf("	<BOAT(%s,%s)>~~~~~WATER~~~~~	", p0, p1);
	}

	for(int i = 0; i < r_mn; i++) {
		printf("M");
	}
	for(int i = 0; i < r_cn; i++) {
		printf("C");
	}
}

int win_condition() {
	return (a_cn == 3 && a_mn == 3) ? LOSS : WIN;
}

void mac_algo() {
    while (win_condition())
    {
        if (!boat_s)
        {
            switch (boat_p)
            {
                case 1: print_status("C", " ");
                        r_cn++;
                        break;
                case 2: print_status("C", "M");
                        r_cn++; r_mn++;    
                        break;   
            }
            
            if (((r_mn - 2) >= r_cn && (a_mn + 2) >= a_cn) || (r_mn - 2) == 0)
            {
                r_mn = r_mn - 2;
                boat_p = 1;
                print_status("M", "M");
                boat_s = 1;
            }
            else if ((r_cn - 2) < r_mn && (a_mn == 0 || (a_cn + 2) <= a_mn) || r_mn == 0)
            {
                r_cn = r_cn - 2;
                boat_p = 2;
                print_status("C", "C");
                boat_s = 1;
            }
   
            else if ((r_cn--) <= (r_mn--) && (a_mn++) >= (a_cn++))
            {
                r_cn = r_cn - 1;
                r_mn = r_mn - 1;
                boat_p = 3;
                print_status("M", "C");
                boat_s = 1;
            }
        }
  
        else
        {
            switch (boat_p)
            {
                case 1: print_status("M", "M");
                        a_mn = a_mn + 2;
                        break;
                
                case 2: print_status("C", "C");
                        a_cn = a_cn + 2;
                        break;   
                
                case 3: print_status("M", "C");
                        a_cn = a_cn + 1;
                        a_mn = a_mn + 1;
                        break;
            }
   
            if (win_condition())
            {
                if (((a_cn > 1 && a_mn == 0) || r_mn == 0))
                {
                    a_cn--;
                    boat_p = 1;
                    print_status("C", " ");
                    boat_s = 0;
                }
                
                else if ((r_cn + 2) > r_mn)
                {
                    a_cn--; a_mn--;
                    boat_p = 2;
                    print_status("C", "M");
                    boat_s = 0;
                }
            }
        }
    }
}

int main() {
	printf("MISSIONARIES AND CANNIBALS\nAIMA - EXERCISE 3.9\n");
	print_status(" ", " ");
	mac_algo();
	print_status(" ", " ");

	return 0;
}